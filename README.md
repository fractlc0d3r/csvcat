_csvcat_ concatenates several homogenous CSV files into a single file.
# Usage
## Drag and drop
### Whole folder
* Drag and drop a folder containing CSV files onto the _csvcat_ icon
* Aternatively, start the _csvcat_ application, then drag and drop a folder containing CSV files into its window.

_output file will be created in the same location as the dropped folder._

### Several files
* Drag and drop several CVS files onto the _csvcat_ icon. 
* Alternatively, start the _csvcat_ applicartion, then drag and drop files into it’s window.

_output file will be created in the same location as the first input file._

## Command line

TD
