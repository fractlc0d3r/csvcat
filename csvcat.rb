#!/usr/bin/ruby
####/usr/bin/env ruby

#require 'pp'
require 'CSV'
require 'optparse'

VERSION = 0.1

options = {}
optparser = OptionParser.new do |opts|
  executable_name = File.basename($PROGRAM_NAME)
  opts.banner = "Usage: #{executable_name} [options] directory | file list"
  opts.on("-o FILE","--outfile FILE", 'Specifies output file name') do |file|
    puts "got file=#{file}"
    options[:outfile] = File.expand_path(file)
  end
  opts.on("--version",'Outputs softwre version number') do |x|
    puts "Version: #{VERSION}"
  end
end

optparser.parse!

if ARGV.empty?
  puts optparser.help
  exit(0)
end

files = File.directory?(ARGV[0]) ? Dir[File.join(ARGV[0], '*.csv')] : ARGV

base = File.basename(ARGV[0])
dir = File.dirname(ARGV[0])

options[:outfile] = File.join(dir,"csvcat_#{base}.csv")

puts "Found #{files.length} input files."

headers = CSV.read(files[0], {headers: true}).headers
puts "Writing #{options[:outfile]}"
CSV.open(options[:outfile], "wb", {headers: headers, write_headers: true}) do |outfile|
  files.each do |infile|
    puts "\t#{infile}"
    CSV.foreach(infile, {headers: true}) {|row| outfile << row}
  end
end

puts 'Done.'

